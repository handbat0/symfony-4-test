<?php

namespace App\Controller\Book;

use App\Entity\Book;
use App\Form\BookType;
use App\Service\BookService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class BookController extends AbstractController {

    /**
     * @Route("/addBook", name="AddBook", methods={"POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function AddBook(Request $request, BookService $bookService) {

        $addBook = $bookService->addBook($request);

        return $this->redirectToRoute('author', [
                    'id' => $addBook['authorId'],
                    'ValidErrors' => $addBook['errors']
        ]);
    }

    /**
     * @Route("/deleteBook/{id}", name="deleteBook", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function deleteBook(Book $book, BookService $bookService) {

        $authorId = $bookService->delete($book);

        return $this->redirectToRoute('author', ['id' => $authorId]);
    }

    /**
     * @Route("/EditBook/{id}", name="editBook", methods={"GET"})
     */
    public function editBook(Book $book) {

        $form = $this->createForm(BookType::class);

        return $this->render('author/editBook.html.twig', [
                    'book' => $book,
                    'editBook' => $form->createView()
        ]);
    }

    /**
     * @Route("/editTitleBook", name="editTitleBook", methods={"POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function editTitleBook(Request $request, BookService $bookService) {

        $addBook = $bookService->editBook($request);

        return $this->redirectToRoute('author', [
                    'id' => $addBook['authorId'],
                    'errors' => $addBook['errors']
        ]);
    }

}
