<?php

namespace App\Controller\Author;

use App\Form\AddAuthorType;
use App\Service\AuthorService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class AddAuthorController extends AbstractController {

    /**
     * @Route("/addAuthor", name="newAuthorPage", methods={"GET", "POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function showFormAddAuthor(Request $request, AuthorService $AuthorService) {

        $form = $this->createForm(AddAuthorType::class); //Создаем форму из конструктива класса 
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $addAuthor = $AuthorService->addAuthor($request);
            if ($addAuthor['add']) {
                return $this->redirectToRoute('mainPage');
            }
        }

        return $this->render('author/addAuthor.html.twig', array(
                    'newAuthor' => $form->createView(),
        ));
    }

}
