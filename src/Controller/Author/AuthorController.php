<?php

namespace App\Controller\Author;

use App\Entity\Author;
use App\Form\BookType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AuthorController extends AbstractController {

    /**
     * @Route("/author/{id}", name="author", methods={"GET"}, requirements={"id"="\d+"})
     */
    public function showAuthorPage(Author $author, $ValidErrors = null) {
        $form = $this->createForm(BookType::class); //Создаем форму из конструктива класса 
        
        return $this->render('author/index.html.twig', [
                    'author' => $author,
                    'ValidErrors' => $ValidErrors,
                    'AddBook' => $form->createView()
        ]);
    }

    /**
     * @Route("/author/{id}/books", name="books", methods={"GET"}, requirements={"id"="\d+"})
     */
    public function showAuthorBooksPage(Author $author) {
        return $this->render('author/books.html.twig', [
                    'author' => $author,
        ]);
    }

}
