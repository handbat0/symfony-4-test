<?php

namespace App\Controller;

use App\Entity\Author;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class MainController extends AbstractController {

    /**
     * @Route("/", name="mainPage")
     */
    public function index() {
        $repository = $this->getDoctrine()->getRepository(Author::class);
        $authors = $repository->findAll(); //Выбрать ВСЕ объекты класса Author 

        return $this->render('main/index.html.twig', [
                    'authors' => $authors,
        ]); //Возвращаем (отображаем) страницу шаблона и передаём переменную $authors
    }

    /**
     * @Route("/login", name="login")
     */
    public function login(Request $request, AuthenticationUtils $authUtils) {
        // получить ошибку входа, если она есть
        $error = $authUtils->getLastAuthenticationError();
        return $this->render('main/login.html.twig', array(
                    'error' => $error,
        ));
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout() {
        return $this->redirectToRoute('mainPage');
    }

}
