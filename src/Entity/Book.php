<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BookRepository")
 */
class Book
{
/**
 * @ORM\Id()
 * @ORM\GeneratedValue()
 * @ORM\Column(type="integer")
 */
private $id;

/**
 * @ORM\ManyToOne(targetEntity="App\Entity\Author", inversedBy="books")
 * @ORM\JoinColumn(nullable=true)
 */
private $author;

/**
 * @ORM\Column(type="string", length=150)
 * @Assert\NotBlank()
 * @Assert\Length(
 *      min = 2,
 *      max = 150,
 *      minMessage = "Название книги не может быть менее {{ limit }} символов.",
 *      maxMessage = "Название книги не может быть длиннее {{ limit }} символов."
 * )
 */
private $title;

public function getId(): ?int
{
return $this->id;
}

public function getAuthor(): Author
{
return $this->author;
}

public function setAuthor(Author $author)
{
$this->author = $author;

return $this;
}

public function getTitle(): ?string
{
return $this->title;
}

public function setTitle(string $Title): self
{
$this->title = $Title;

return $this;
}

}
