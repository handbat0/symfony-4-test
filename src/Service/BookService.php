<?php

namespace App\Service;

use App\Entity\Author;
use App\Entity\Book;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class BookService {

    private $entityManager;
    private $validator;

    public function __construct(EntityManagerInterface $entityManager, ValidatorInterface $validator) {
        $this->entityManager = $entityManager;
        $this->validator = $validator;
    }

    public function delete(Book $book) {
        $this->entityManager->remove($book);
        $this->entityManager->flush();
        return $this->getAuthorId($book);
    }

    public function addBook(Request $request) {
        $book = $this->newBook($request);
        $valid = $this->validationBook($book);

        if (!$valid['valid']) {
            return array(
                'authorId' => $valid['authorId'],
                'errors' => $valid['errors']
            );
        }

        $this->entityManager->persist($valid['book']);
        $this->entityManager->flush();

        return array(
            'authorId' => $valid['authorId'],
            'errors' => null
        );
    }

    public function editBook(Request $request) {
        $book = $this->updateTitleBook($request);
        $valid = $this->validationBook($book);
        if (!$valid['valid']) {
            return array(
                'authorId' => $valid['authorId'],
                'errors' => $valid['errors']
            );
        }

        $this->entityManager->persist($valid['book']);
        $this->entityManager->flush();

        return array(
            'authorId' => $valid['authorId'],
            'errors' => null
        );
    }

    public function updateTitleBook(Request $request) {
        $aBook = $request->request->get('book');
        $repository = $this->entityManager->getRepository(Book::class);
        $book = $repository->find($aBook['id']);
        $book->setTitle($aBook['title']);
        return $book;
    }

    public function newBook(Request $request) {
        $newBook = $request->request->get('book');

        $repository = $this->entityManager->getRepository(Author::class);
        $author = $repository->find($newBook['author']);

        $book = new Book();
        $book->setTitle($newBook['title']);
        $book->setAuthor($author);
        return $book;
    }

    public function validationBook(Book $book) {
        $authorId = $this->getAuthorId($book);
        $errors = $this->validator->validate($book);
        if (count($errors) > 0) {
            $errorsString = (string) $errors;
            return array(
                'valid' => false,
                'authorId' => $authorId,
                'errors' => $errorsString
            );
        }

        return array(
            'valid' => true,
            'authorId' => $authorId,
            'book' => $book
        );
    }

    public function getAuthorId(Book $book) {
        $authorId = $book->getAuthor()->getId();
        return $authorId;
    }

}
