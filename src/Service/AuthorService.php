<?php

namespace App\Service;

use App\Entity\Author;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AuthorService {

    private $entityManager;
    private $validator;

    public function __construct(EntityManagerInterface $entityManager, ValidatorInterface $validator) {
        $this->entityManager = $entityManager;
        $this->validator = $validator;
    }

    public function addAuthor(Request $request) {

        $author = $this->newAuthor($request);
        $valid = $this->validationAuthor($author);

        if (!$valid['valid']) {
            return array(
                'add' => false,
                'errors' => $valid['errors']
            );
        }

        $this->entityManager->persist($valid['author']);
        $this->entityManager->flush();

        return array(
            'add' => true,
            'errors' => null
        );
    }

    public function newAuthor(Request $request) {
        $newAuthor = $request->request->get('add_author');
        $author = new Author();
        $author->setName($newAuthor['Name']);
        $author->setSurname($newAuthor['Surname']);
        return $author;
    }

    public function validationAuthor(Author $author) {
        $errors = $this->validator->validate($author);
        if (count($errors) > 0) {
            $errorsString = (string) $errors;
            return array(
                'valid' => false,
                'errors' => $errorsString
            );
        }

        return array(
            'valid' => true,
            'author' => $author
        );
    }

}
